import { Component, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  searchText = '';
  searchTextChanged: Subject<string> = new Subject<string>();
  @Output() enteredText = new EventEmitter();

  constructor() {
  this.searchTextChanged.pipe(
      debounceTime(300),
      distinctUntilChanged())
      .subscribe(model => {
        console.log(model);
        this.searchText = model;
      });
  }

  ngOnInit(): void {
  }


  onSearchTextUpdate(value: string) { // updating the search value according to change
    this.searchTextChanged.next(value);
  }
  submitSearch() {
    this.enteredText.emit(this.searchText);
  }
}
