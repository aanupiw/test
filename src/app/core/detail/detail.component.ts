import { Component, OnInit, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { DetailService } from 'src/app/services/detail.service';
import * as marked from 'marked';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  data: any;
  noReadMe = false;
  @ViewChild('myDiv') abc: ElementRef; // accesssing the dom element

  constructor(private detailService: DetailService,
              private renderer: Renderer2,
              private elementRef: ElementRef) { }

  ngOnInit(): void {
    this.detailService.data.subscribe(
      (res: any) => {
        this.data = res;
      }
    );
    this.detailService.getReadMe(this.data.owner.login, this.data.name).subscribe(
      (res: any) => {
        this.convert(atob(res.content)); // decoding base64
      },
      err => {
        this.noReadMe = true;
      }
    );
  }

  convert(markdown) {
    const changed = marked(markdown); // converting markdown to html
    this.renderer.setProperty(this.abc.nativeElement, 'innerHTML', changed); // rendering the markup as html
  }
}
