import { Component, OnInit } from '@angular/core';
import { RepoService } from 'src/app/services/repo.service';
import { DetailService } from 'src/app/services/detail.service';
import { Router, Params, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  page = 1;
  pageSize = 10;
  collectionSize = 0;
  repos: any;
  searchStarted = false;
  searching = false;
  searchText = '';

  queryParams: Params = {
    q: this.searchText,
    page: this.page,
    per_page: 3
  };


  constructor(private repoService: RepoService,
              private detailService: DetailService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }


  ngOnInit(): void {
    this.repoService.data.subscribe(
      (data: any) => {
        if (data) { // maintaining the state during back
          this.setQuery(data.q, data.page);
          this.page = data.page;
          this.getlist();
          this.addQueryParams();
        } else {
          if (this.router.url !== '/') {  // maintaining the  state even after refresh
            const urls = [];
            const url = this.router.url.split('&');
            url.forEach((datas: any , i = datas.length) => {
             const splittedData = url[i].split('=');
             urls.push(splittedData[1]); } );
            this.setQuery(urls[0], urls[1]);
            this.page = urls[1];
            this.getlist();
          }
        }
      }
    );
  }


  getlist(resetPage = false) {
    this.searching = true;
    this.repoService.getList(this.queryParams).subscribe(
      (res: any) => {
        this.searchStarted = true;
        this.searching = false;
        this.repos = res.items;
        this.collectionSize = res.total_count;
      },
      err => {
        this.collectionSize = 0;
        this.searching = false;
        this.searchStarted = false;
        this.repos = [];
      }
    );
  }

  pageChange(page) {
    this.queryParams.page = page;
    this.getlist();
    this.addQueryParams();
  }

  detailView(item) {
    this.detailService.setData(item);
    this.repoService.setData(this.queryParams);
    // this.router.navigate(['detail'], { skipLocationChange: true }); // encapsulation
    this.router.navigate(['detail', item.name]);
  }

  onTextChange(event) {
    this.searchText = event;
    this.setQuery(event, this.page);
    this.addQueryParams();
    this.getlist();

  }


  addQueryParams() {     // method to add query params
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: this.queryParams,
        queryParamsHandling: 'merge', // remove to replace all query params by provided
      });
  }

  setQuery(query, page) {
    this.queryParams.q = query;
    this.queryParams.page = page;
    this.queryParams.per_page = 10;
  }

}
