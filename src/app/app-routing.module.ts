import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './core/list/list.component';
import { DetailComponent } from './core/detail/detail.component';


const routes: Routes = [
  {path: '', component: ListComponent},
  {path: 'detail/:name', component: DetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
