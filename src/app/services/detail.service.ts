import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DetailService {

   private detail =  new BehaviorSubject<string>('');
   data = this.detail.asObservable();

   setData(data) {
    this.detail.next(data);
  }

  constructor(private http: HttpClient) { }

  getReadMe(owner, name){
    return this.http.get(`https://api.github.com/repos/${owner}/${name}/readme`);
  }
}
