import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DetailService } from './detail.service';
import { HttpClient} from '@angular/common/http';

describe('DetailService', () => {
  let service: DetailService;
  let httpClient: HttpClient;
  let httpMock: HttpTestingController ;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DetailService]
    });
    service = TestBed.inject(DetailService);
    httpClient = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController);
  });


  const detail = {
    id: 1,
    name: 'Asd',
    content : btoa('adfasdf')
  };

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getReadme() should return data', () => {
    service.getReadMe('abc', 'xyz').subscribe((res: any) => {
      expect(res).toEqual(detail);
      expect(res.name).toBe('Asd');
    });
    const req = httpMock.expectOne('https://api.github.com/repos/abc/xyz/readme');
    expect(req.request.method).toEqual('GET');
    req.flush(detail);
  });

  it('test readme' , () => {
    httpClient.get('https://api.github.com/repos/abc/xyz/readme').subscribe((res: any) => {
      expect(res).toEqual(detail);
      expect(res.name).toBe('Asd');
    });
    const req = httpMock.expectOne('https://api.github.com/repos/abc/xyz/readme');
    expect(req.request.method).toEqual('GET');
    req.flush(detail);
  } );

  afterEach(() => {
    httpMock.verify();
  });

});


