import { Injectable } from '@angular/core';
import {HttpClient, HttpBackend} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RepoService {

  constructor(private http: HttpClient) { }


  private list =  new BehaviorSubject<string>('');
   data = this.list.asObservable();

   setData(data) {
    this.list.next(data);
  }

  getList(params){
    return this.http.get(`${environment.API_URL}search/repositories`, {params: {
      ...params
    }});
  }
}
