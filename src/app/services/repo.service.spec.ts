import { TestBed } from '@angular/core/testing';

import { RepoService } from './repo.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

describe('RepoService', () => {
  let service: RepoService;
  let httpClient: HttpClient;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RepoService]
    });
    service = TestBed.inject(RepoService);
    httpClient = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get data', () => {
    const testData = {name: 'Test Data'};
    httpClient.get('https://api.github.com/search/repositories')
    .subscribe((data: any) =>
      expect(data).toEqual(testData)
    );
    const req = httpMock.expectOne('https://api.github.com/search/repositories');
    expect(req.request.method).toEqual('GET');
    req.flush(testData);
    });

  it('should be able to get data from service', () => {
    const testData = {name: 'Test Data'};
    service.getList({q: 'a' }).subscribe( (data: any) => {
      expect(data).toEqual(testData);
      expect(data.name).toBe('Test Data');
  });
    const req = httpMock.expectOne('https://api.github.com/search/repositories?q=a');
    expect(req.request.method).toEqual('GET');
    req.flush(testData);
  });
});
