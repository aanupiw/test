# Test

This project can be used to view the github repositories and its detail.

## Installation

After extracting or cloning the project use npm install to install its dependencies.

## Running

Run `ng serve -o` to view it on browser.

## Testing
Run `ng test` to run test cases.
